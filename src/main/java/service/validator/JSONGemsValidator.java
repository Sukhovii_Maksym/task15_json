package service.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;

import java.io.File;
import java.io.IOException;

public class JSONGemsValidator {

    public boolean isValidJsonGems()
            throws IOException, ProcessingException {
        final JsonNode d = JsonLoader.fromFile(new File("src/main/resources/json/gems.json"));
        final JsonNode s = JsonLoader.fromFile(new File("src/main/resources/json/gemsSchema.json"));
        final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        JsonValidator v = factory.getValidator();
        ProcessingReport report = v.validate(s, d);
        return report.toString().contains("success");
    }
}
