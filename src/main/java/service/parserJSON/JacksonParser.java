package service.parserJSON;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import domain.Gem;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;

public class JacksonParser {
    private ObjectMapper objectMapper;

    public JacksonParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Gem> getGemsList() {

        Gem[] gems = new Gem[0];
        try {
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            objectMapper.setVisibilityChecker(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));
            gems = objectMapper.readValue(new File("src/main/resources/json/gems.json"), Gem[].class);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Arrays.asList(gems);
    }

    public void writeToFile() {
        try {
            Writer writer = new FileWriter("src/main/resources/out/JacksonOut.txt");
            getGemsList().forEach(g -> {
                try {
                    writer.write(g.toString());
                    writer.append(System.getProperty("line.separator"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
